const { Chirp, toAscii } = ChirpConnectSDK;
const chirpInstance = Chirp({
  key: '81dB8e046cc6A68e59fe24Adc',
  onReceived: data => {
    console.log('Receiving Data');
    if (data.length > 0) {
      const ascii = toAscii(data);
      console.log(ascii);
      addReceivedData(ascii);
    } else {
      console.error('Decode Failed');
    }
  }
});
function sendData() {
  chirpInstance.then(sdk => {
    content = document.getElementById('data_to_send').value;
    const message = { name: 'Viz', content };
    sdk.send(content);
  }).catch(console.error);
}
function receiveData() {}
function addReceivedData(text) {
  const container = document.createElement('div');
  const timestamp = document.createElement('span');
  timestamp.innerHTML = new Date().toLocaleString();
  container.appendChild(timestamp);
  const message = document.createElement('span');
  message.innerHTML = ' : ' + text;
  container.appendChild(message);
  document.getElementById('received_data').appendChild(container);
}